#!/usr/bin/env python
# control_fan.sh: A script to control a fan connected to a rock pi 4

# Based upon the following sources:
# http://www.instructables.com/id/Automated-cooling-fan-for-Pi/
# https://hackernoon.com/how-to-control-a-fan-to-cool-the-cpu-of-your-raspberrypi-3313b6e7f92c

##
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed WITHOUT ANY WARRANTY;
#  without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

import os
import time
import signal
import sys
#import RPi.GPIO as GPIO
import math

# The pin ID (https://www.raspberrypi.org/documentation/usage/gpio/README.md)
PIN_NO = 12
# The PIN GPIO NO (https://wiki.radxa.com/Rockpi4/hardware/gpio)
PIN_GPIO_NO = 131
# Start fan if temp > start temp
FAN_START = 65
# Stop fan if temp < end temp
FAN_END = 40
# Time to wait for before switching fan on / off
WAIT_INTERVAL = 30

def setup():
#    GPIO.setmode(GPIO.BCM)
#    GPIO.setwarnings(False)
#    GPIO.setup(PIN, GPIO.OUT)
    os.system("[ ! -d /sys/class/gpio/gpio131 ] && echo 131 > /sys/class/gpio/export")
#    GPIO.output(PIN, GPIO.LOW)
    os.system("echo out > /sys/class/gpio/gpio131/direction")
    return()

def get_temperature():
    res = os.popen('cat /sys/class/thermal/thermal_zone0/temp').readline()
    temp = math.ceil(int(res) / 1000)
    return temp

def fanON():
    #GPIO.setup(PIN, GPIO.OUT)
    #echo echo out > /sys/class/gpio/gpio131/direction
    #setPin(GPIO.HIGH)
    os.system("echo 1 > /sys/class/gpio/gpio131/value")
    return()

def fanOFF():
    #setPin(GPIO.LOW)
    os.system("echo 0 > /sys/class/gpio/gpio131/value")
    #GPIO.setup(PIN, GPIO.IN)
    return()

def run():
    temp = get_temperature()
    CPU_temp = float(temp)
    print(time.strftime('%Y-%m-%d %X') + ' , temp is ' + temp)
    if CPU_temp > FAN_START:
        if check_fan(PIN) != GPIO.HIGH:
            print('Turning fan on')
        # Since the input read can be wrong, run the function nevertheless
        fanON()
    elif CPU_temp < FAN_END:
        if check_fan(PIN) == GPIO.HIGH:
            print('Turning fan off')
        fanOFF()
    else:
        # nothing to do
        pass
    #print check_fan(PIN)
    return()

def setPin(mode):
    #GPIO.output(PIN, mode)
    #os.system("echo mode > /sys/class/gpio/gpio131/direction")
    return()

def check_fan(pin):
    #return GPIO.input(pin)
    res = os.popen('cat /sys/class/gpio/gpio131/value')
    return res

def cleanUP():
    GPIO.cleanup()

# Get the action. For manually turning on/off the fan
action = sys.argv.pop()

if action == "on" or action == "start":
    print ("Turning fan on")
    setup()
    fanON()
    exit()
elif action == "off" or action == "stop":
    print ("Turning fan off")
    setup()
    fanOFF()
    exit()
elif action == "status":
    temp = get_temperature()
    print('Temp is ' + str(temp))
    CPU_temp = float(temp)
    if CPU_temp > FAN_START:
        print('Fan should be on')
    elif CPU_temp < FAN_END:
        print('Fan should be off')
    else:
        # fan can be on or off
        print('Fan state unknown')
    exit()

# Handle signal (http://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python)
signal.signal(signal.SIGINT, signal.default_int_handler)

try:
    setup()
    while True:
        run()
        time.sleep(WAIT_INTERVAL)
except KeyboardInterrupt:
    # trap a CTRL+C keyboard interrupt
    # resets all GPIO ports used by this program
    GPIO.cleanup()
finally:
    GPIO.cleanup()
